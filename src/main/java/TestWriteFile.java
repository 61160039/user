
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author james
 */
public class TestWriteFile {

    public static void main(String[] args) {

        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        User user1 = new User("user1", "password");
        User user2 = new User("user2", "password");
        User admin = new User("admin", "admin");
        ArrayList<User> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(admin);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }
}
