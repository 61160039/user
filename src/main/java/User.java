
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author james
 */
public class User implements Serializable{

    private String loginName;
    private String passwordl;

    public User(String loginName, String passwordl) {
        this.loginName = loginName;
        this.passwordl = passwordl;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPasswordl() {
        return passwordl;
    }

    public void setPasswordl(String passwordl) {
        this.passwordl = passwordl;
    }

    @Override
    public String toString() {
        return "User:" + "username: " + loginName + ", password: " + passwordl ;
    }
    
}
